function add(num)
{
    var sum = 0;
    num.map(temp=> sum+=temp);
    return sum;

}

var numbers = [1,2,3,5,6,8];

// console.log(add(numbers[1], numbers[6]));
// console.log(add(1,2));

// spread operator 

function addnumbers(... num)
{
    var sum = 0;
    num.map(temp=> sum+=temp);
    return sum;  
}

console.log(addnumbers(1,2,3));
// console.log(addnumbers)
console.log(addnumbers(numbers));
console.log(addnumbers(numbers[1], numbers[6]));
console.log(addnumbers(1,2,2,23,2,3,23,2,3,2,32,3,23));