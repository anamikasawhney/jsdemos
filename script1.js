var x = 10;
function B()
{
    console.log(x);
    var x = 20;
    console.log(x);
}
B();
//  {  scope }
console.log(x);

// var has global scope
// let has function / block scope

let y = 10;
function A() 
{
    console.log("value of y " + y);
    let y = 20;
    console.log(y);
}

// A();
console.log("Outside" + y);

const z = 1000;
z=9090;
console.log(z)

