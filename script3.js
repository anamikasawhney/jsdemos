console.log("Arrow functions")
// arrow function are a short hand way to write functions
// we have to use => // => Arrow opeartor
// 

// function hello()
// {
//     console.log("hello");
// }

// hello();

hello = () =>
{ 
    console.log("hello");
}


hello();

hello1 = () => console.log("hello1");
hello1();

function add()
{
    return 2+3;
     
}

add = ()=> {return 2+3};

function add2(no1, no2)
{
     return no1+no2;
}


add3 = (no1,no2)=> {return no1+no2;}

add4 = (no1,no2)=> no1+no2;

console.log("Sum is " + add4(1,2));

